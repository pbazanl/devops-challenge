pipeline{
    agent{
        label "pro-multitask-agent"
    }
    options{
        timeout(activity: true, time: 2, unit: 'HOURS')
        parallelsAlwaysFailFast()
        disableConcurrentBuilds()
    }
    triggers {
            cron "H 7 * * 0-5 \nH 13 * * 0-5"
    }
    environment{
        SSH_CLONE_URL = "ssh://git@bitbucket.org:chicho2020/devops-challenge.git"
    }
    parameters{
        gitParameter(branch: '',
                    branchFilter: 'origin/(.*)',
                    defaultValue: 'feature/pedro.bazan',
                    description: '',
                    name: 'BRANCH',
                    quickFilterEnabled: true,
                    selectedValue: 'NONE',
                    sortMode: 'NONE',
                    tagFilter: '*',
                    useRepository: 'ssh://git@bitbucket.org:chicho2020/devops-challenge.git',
                    type: 'PT_BRANCH')
    }
    stages{
        stage("Checkout"){
            steps{
                cleanWs()
                script{
                    def lastCommit = ""
                    git poll: true, branch: BRANCH, credentialsId: 'myrepocreds', url: SSH_CLONE_URL
                    lastCommit = sh(returnStdout: true, script: "git log --oneline | awk '{print \$1}' | head -1").trim()
                    branchLName = BRANCH
                    currentBuild.description = "$branchLName | $lastCommit"
                }
            }
        }
        stage("Docker"){
            steps{
                script{
                	//building docker image
                    sh "docker build . -t pedro.bazan/node-web-app"
                    //showing top level images, their repository, tags and size
                    sh "docker images"
                    //starting
                    sh "docker run -p 17771:3000 -d pedro.bazan/node-web-app"
                    //get container ID
                    sh "docker ps"
                    //prints STDOUT and STDERR
                    sh "docker logs <container id>"
                    //enter the container
                    sh "docker exec --user guest -it <container id> /bin/bash"
                    //test
                    sh "curl -i localhost:17771"
                }
            }
        }
        stage("Nomad"){
            steps{
                script{
                	//dry-run
                	sh "nomad job plan deploy.nomad"
                	//submiting new and existing jobs to nomad
                	sh "nomad job run deploy.nomad"
                	//dry-run
                	sh "nomad job plan deploy.nomad"
                	//ensuring job was not changed
                	sh "nomad job run -check-index 131 deploy.nomad"
                }
            }
        }
        stage("Terraform"){
            steps{
                script{
                	//previamente configurar sh "aws configure"

                	//initialize working directory
                	sh "terraform init"
                	//evaluation desired state with declared resources
                	sh "terraform plan"
                	//performing the plan
                	sh "terraform apply"
                }
            }
        }
    }
    post{
        always{
            echo "========always========"
        }
        success{
            echo "========pipeline executed successfully ========"
        }
        failure{
            echo "========pipeline execution failed========"
        }
    }
}