// definition of version
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

//definition of aws region and AWS profile
provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

//Attachment of EBS volume to ec2 instance
resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.example.id
  instance_id = aws_instance.app_server.id
}

//attachment of security group to ec2 instance
resource "aws_network_interface_sg_attachment" "sg_attachment" {
  security_group_id    = aws_security_group.sample_network.id
  network_interface_id = aws_instance.app_server.primary_network_interface_id
}

//definition of aws EC2 instance
resource "aws_instance" "app_server" {
  ami           = "ami-03190fe20ef6b1419"
  availability_zone = "us-east-1a"
  instance_type = "t3a.nano"

  tags = {
    Name = "Automated AWS EC2 instance with Terraform"
  }
}

//definition of aws ebs disk volume
resource "aws_ebs_volume" "example" {
  availability_zone = "us-east-1a"
  size              = 20
}

//definition of aws security group
resource "aws_security_group" "sample_network" {
  name        = "sample_network"
  description = "Sample Network inbound traffic"
  vpc_id      = aws_vpc.main.id

  ingress {
    description      = "TLS from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["11.22.33.44/32"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "Automated AWS Security Group with Terraform"
  }
}